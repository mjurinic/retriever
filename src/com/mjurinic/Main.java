package com.mjurinic;

import MyUdpServer.*;
import messagetypes.*;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Main {

    public static Config ServerConfig;
    public static HashMap<String, OnlineStreamInfo> StreamDB = new HashMap<String, OnlineStreamInfo>();

    // TTL is set to 15min (900s) as servers default value
    public static final short DATA_TTL = 900;

    private static UdpServer server;
    private static String timestamp;

    /**
     * Missing auto stream removal after XX time
     */
    private static List<SocketIdentifier> reachableRelays = new ArrayList<SocketIdentifier>();

    public static void main(String[] args) {
        init();
        handleRequests();
    }

    private static void handleRequests() {
        System.out.println("LISTENING FOR REQUESTS");
        System.out.println("----------------------");

        server.setSocketTimeOut(0);

        while (true) {
            ReceivedPacket packet = server.receivePacket();
            Object obj = null;

            timestamp = new java.text.SimpleDateFormat("h:mm:ss a").format(new Date());

            try {
                obj = SerializedObject.Deserialize(packet.getData());

                InetAddress remoteIp = packet.getIp();
                int remotePort = packet.getPort();

                server.setReceiverIP(remoteIp);
                server.setReceiverPort(remotePort);

                System.out.println("["+ timestamp +"] Request received from [" + remoteIp + ":" + remotePort + "]");

                /*
                 * MSG_STREAM_ADVERTISEMENT
                 */
                if (obj instanceof StreamAdvertisement) {
                    System.out.println("["+ timestamp +"] Message type: MSG_STREAM_ADVERTISEMENT");

                    StreamAdvertisement msg = (StreamAdvertisement) obj;
                    String identifier = msg.getIdentifier();

                    if (identifier == null) {
                        server.sendPacket(remoteIp, remotePort, SerializedObject.Serialize(new IdentifierNotUsable(remoteIp, remotePort, identifier)));
                        continue;
                    }

                    if (StreamDB.containsKey(identifier)) {
                        System.out.println("Key exists");

                        OnlineStreamInfo currData = StreamDB.get(identifier);

                        boolean equalIp = currData.getIp().equals(msg.getIp());
                        boolean equalPort = (currData.getPort() == msg.getPort());

                        if (equalIp && equalPort) {
                            StreamDB.get(identifier).setTimestamp(System.currentTimeMillis() / 1000L);
                            server.sendPacket(remoteIp, remotePort, SerializedObject.Serialize(new StreamRegister(remoteIp, remotePort)));
                        }
                        else {
                            server.sendPacket(remoteIp, remotePort, SerializedObject.Serialize(new IdentifierNotUsable(remoteIp, remotePort, identifier)));
                        }
                    }
                    else {
			System.out.println("[REGISTERING] StreamSource -> " + remoteIp +":"+ remotePort);

                        StreamDB.put(identifier, new OnlineStreamInfo(remoteIp, remotePort, msg));
                        server.sendPacket(remoteIp, remotePort, SerializedObject.Serialize(new StreamRegister(remoteIp, remotePort)));
                    }
                }

                /*
                 * MSG_FIND_STREAM_SOURCE
                 */
                else if (obj instanceof FindStreamSource) {
                    System.out.println("["+ timestamp +"] Message type: MSG_FIND_STREAM_SOURCE");

                    FindStreamSource msg = (FindStreamSource) obj;

                    String identifier = msg.getIdentifier();

                    if (StreamDB.containsKey(identifier)) {
                        SocketIdentifier playerInfo = new SocketIdentifier(remoteIp, remotePort);
                        StreamSourceData streamSourceData = new StreamSourceData(playerInfo, StreamDB.get(identifier));

                        server.sendPacket(remoteIp, remotePort, SerializedObject.Serialize(streamSourceData));
                    }
                    else {
                        /*
                         * If no record is found, the message is discarded
                         * Maybe we could add another message type to handle this case
                         */
                    }
                }

                /*
                 * MSG_STREAM_REMOVE
                 */
                else if (obj instanceof StreamRemove) {
                    System.out.println("["+ timestamp +"] Message type: MSG_STREAM_REMOVE\n");

                    StreamRemove msg = (StreamRemove) obj;

                    String identifier = msg.getIdentifier();

                    if (StreamDB.containsKey(identifier)) {
                        OnlineStreamInfo registeredStream = StreamDB.get(identifier);

                        if (remoteIp.equals(registeredStream.getIp()) && remotePort == registeredStream.getPort()) {
                            StreamDB.remove(identifier);
                        }
                        else {
                            /*
                             * Ignore it, request was sent from a different device
                             */
                        }
                    }
                    else {
                        /*
                         * Ignore it, stream id is non existent
                         */
                    }
                }

                /**
                 * MSG_RELAY_REQUEST
                 */
                else if (obj instanceof RelayListRequest) {
                    System.out.println("["+ timestamp +"] Message type: MSG_RELAY_LIST_REQUEST");

                    RelayList relayList = new RelayList();

                    for (int i = 0; i < reachableRelays.size(); ++i) {
                        relayList.addRelayServer(reachableRelays.get(i));
                    }

                    server.sendPacket(remoteIp, remotePort, SerializedObject.Serialize(relayList));
                }

                /**
                 * UNKNOWN MESSAGE
                 */
                else {
                    System.out.println("["+ timestamp +"] UNKNOWN MESSAGE.");
                }
            }
            catch (NullPointerException e) {
                //
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void init() {
        System.out.println("\n\n----------------------------");
        System.out.println("STARTING INITIALIZATION FAZE");
        System.out.println("----------------------------\n");

        ServerConfig = new Config();
        server = new UdpServer(9090);

        List<SocketIdentifier> relayList = ServerConfig.getRelayList();

        for (int i = 0; i < relayList.size(); ++i) {
            SocketIdentifier relayServer = relayList.get(i);

            System.out.println("RELAY SERVER #" + (i + 1) + ":");
            pingRelay(relayServer);
        }

        System.out.println("----------------------------");
        System.out.println("INITIALIZATION FAZE FINISHED");
        System.out.println("----------------------------\n");
    }

    private static void pingRelay(SocketIdentifier relayServer) {
        server.setReceiverIP(relayServer.getIp());
        server.setReceiverPort(relayServer.getPort());
        server.setSocketTimeOut(1000);

        byte[] serializedData = SerializedObject.Serialize(new Ping());

        System.out.println("Pinging [" + relayServer.getIp() + ":" + relayServer.getPort() + "]...");

        server.sendPacket(serializedData);
        server.receivePacket();

        long latency = server.getLatency();

        if (latency < 0) {
            System.out.println("Latency: *TIMEOUT*\n");
        }
        else {
            reachableRelays.add(relayServer);
            System.out.println("Latency: " + latency + "ms\n");
        }
    }
}
