package com.mjurinic;

import messagetypes.RelayServerInfo;
import messagetypes.SocketIdentifier;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 20.07.15..
 */
public class Config {

    private InetAddress localIp;
    private int localPort;
    private List<SocketIdentifier> relayList;

    public Config() {
        relayList = new ArrayList<SocketIdentifier>(){};
        parseJson(readFromFile());
    }

    public InetAddress getLocalIp() {
        return localIp;
    }

    public int getLocalPort() {
        return localPort;
    }

    public List<SocketIdentifier> getRelayList() {
        return relayList;
    }

    private String readFromFile() {
        BufferedReader br = null;
        String ret = null;

        try {
            URL url = getClass().getResource("server.ini"); 
                       
            FileReader fr = new FileReader(url.getPath());
            br = new BufferedReader(fr);

            StringBuilder sb = new StringBuilder();
			String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }

            ret = sb.toString();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
            	if (br != null)
                	br.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }

        return ret;
    }

    private void parseJson(String jsonString) {
        try {
            JSONObject obj = new JSONObject(jsonString);

            localIp = InetAddress.getByName((String) obj.get("local_ip"));
            localPort = obj.getInt("local_port");

            JSONArray relays = (JSONArray) obj.get("relay_list");

            for (int i = 0; i < relays.length(); ++i) {
                JSONObject relay = relays.getJSONObject(i);

                InetAddress ip = InetAddress.getByName(relay.getString("ip"));
                int port = relay.getInt("port");

                relayList.add(new SocketIdentifier(ip, port));
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
