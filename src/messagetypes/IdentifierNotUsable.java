package messagetypes;

import java.io.Serializable;
import java.net.InetAddress;

/**
 * Public IP and port number of the source
 * Variables are set by the server
 */
public class IdentifierNotUsable extends SocketIdentifier implements Serializable {

    private byte type;

    public IdentifierNotUsable(InetAddress ip, int port, String identifier) {
        super(identifier, ip, port);

        type = MessageType.MSG_IDENTIFIER_NOT_USABLE.getValue();
    }
}
