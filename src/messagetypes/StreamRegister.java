package messagetypes;

import java.io.Serializable;
import java.net.InetAddress;

/**
 * Contains public IP address and port number of the source
 * IP address and port number are determined by the server
 */
public class StreamRegister extends SocketIdentifier implements Serializable {

    private byte type;
    private short TTL;

    public StreamRegister(InetAddress ip, int port) {
        super(ip, port);

        type = MessageType.MSG_STREAM_REGISTERED.getValue();
        TTL = 900;
    }

    public short getTTL() {
        return TTL;
    }

    public void setTTL(short TTL) {
        this.TTL = TTL;
    }
}
