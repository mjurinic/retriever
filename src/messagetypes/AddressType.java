package messagetypes;

public enum AddressType {

    ADR_IPv4((byte)0x01),
    ADR_IPv6((byte)0x02),
    ADR_HOSTNAME((byte)0x03);

    private byte type;

    private AddressType(byte type) {
        this.type = type;
    }

    public byte getValue() {
        return this.type;
    }
}
