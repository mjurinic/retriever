package messagetypes;

import java.io.Serializable;

/**
 * Source -> Relay when 'clients' are behind NAT
 *
 * Contains public ip and port num. from the player device
 */

public class PleaseForward implements Serializable {

    private Object object;
    private String identifier, ip;
    private int port;

    public PleaseForward(String identifier, String ip, int port, Object object) {
        this.object = object;
        this.ip = ip;
        this.identifier = identifier;
        this.port = port;
    }

    public Object getObject() {
        return object;
    }

    public String getIp() { return ip; }

    public int getPort() { return port; }
}
