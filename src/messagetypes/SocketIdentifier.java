package messagetypes;

import java.io.Serializable;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;

public class SocketIdentifier implements Serializable {

    private String identifier;
    private byte IPtype;
    private InetAddress ip;
    private int port;

    public SocketIdentifier(InetAddress ip, int port) {
        this.ip = ip;
        this.port = port;

        if (ip.toString().split("/")[0].length() > 1) {
            this.IPtype = AddressType.ADR_HOSTNAME.getValue();
        }
        else if (ip instanceof Inet4Address) {
            this.IPtype = AddressType.ADR_IPv4.getValue();
        }
        else if (ip instanceof Inet6Address) {
            this.IPtype = AddressType.ADR_IPv6.getValue();
        }
    }

    public SocketIdentifier(String identifier, InetAddress ip, int port) {
        this.ip = ip;
        this.port = port;
        this.identifier = identifier;

        if (ip.toString().split("/")[0].length() > 1) {
            this.IPtype = AddressType.ADR_HOSTNAME.getValue();
        }
        else if (ip instanceof Inet4Address) {
            this.IPtype = AddressType.ADR_IPv4.getValue();
        }
        else if (ip instanceof Inet6Address) {
            this.IPtype = AddressType.ADR_IPv6.getValue();
        }
    }

    public byte getIPtype() { return IPtype; }

    public InetAddress getIp() { return ip; }

    public int getPort() { return port; }

    public String getIdentifier() { return identifier; }

    public void setIdentifier(String identifier) { this.identifier = identifier; }
}
