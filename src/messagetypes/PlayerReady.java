package messagetypes;

import java.io.Serializable;
import java.net.InetAddress;


public class PlayerReady implements Serializable {

    private SocketIdentifier publicAddress;
    private SocketIdentifier localAddress;
    private String identifier;

    public PlayerReady(String identifier, SocketIdentifier publicAddress, SocketIdentifier localAddress) {
        this.identifier = identifier;
        this.publicAddress = publicAddress;
        this.localAddress = localAddress;
    }

    public String getIdentifier() { return identifier; }

    public SocketIdentifier getPublicAddress() { return publicAddress; }

    public SocketIdentifier getLocalAddress() { return localAddress; }
}
